with open('input.txt') as f:
    data = f.read().splitlines()

print( len([n for i, n in enumerate(data) if int(n) > int(data[i-1])]) )

increases = 0

with open('res.txt', 'w') as f:
    for i, n in enumerate(data):
        if int(n) > int(data[i-1]):
            increases += 1

print(increases)