with open('input.txt') as f:
    data = f.read().splitlines()

print( len([n for i, n in enumerate(data[:-3]) if int(n)+int(data[i+1])+int(data[i+2]) < int(data[i+1])+int(data[i+2])+int(data[i+3])]) )

increases = 0

with open('res.txt', 'w') as f:
    for i, n in enumerate(data[:-3]):
        if int(n)+int(data[i+1])+int(data[i+2]) < int(data[i+1])+int(data[i+2])+int(data[i+3]):
            increases += 1

print(increases)