with open('input.txt') as f:
    data = f.read().splitlines()

coords = [0, 0]

for coord in data:
    h, d = coord.split(' ')
    if h == 'forward':
        coords[0] += int(d)
    elif h == 'up':
        coords[1] -= int(d)
    elif h == 'down':
        coords[1] += int(d)

print(coords)
print(coords[0] * coords[1])