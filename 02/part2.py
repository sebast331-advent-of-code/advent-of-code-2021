with open('input.txt') as f:
    data = f.read().splitlines()

# horizontal, depth, aim
coords = [0, 0, 0]

for coord in data:
    h, d = coord.split(' ')
    if h == 'forward':
        coords[0] += int(d)
        coords[1] += coords[2] * int(d)
    elif h == 'up':
        coords[2] -= int(d)
    elif h == 'down':
        coords[2] += int(d)

print(coords)
print(coords[0] * coords[1])