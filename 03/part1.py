with open('input.txt') as f:
    data = f.read().splitlines()

gamma = ''
epsilon = ''

for j in range(len(data[0])):
    one_or_zero = 0
    for i in range(len(data)):
        if data[i][j] == '1':
            one_or_zero += 1
        else:
            one_or_zero -= 1
    if one_or_zero < 0:
        gamma += '0'
        epsilon += '1'
    else:
        gamma += '1'
        epsilon += '0'

# Binary to dec
print(int(gamma, 2) * int(epsilon, 2))