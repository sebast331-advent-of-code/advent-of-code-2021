with open('input.txt') as f:
    data = f.read().splitlines()

oxygen = data.copy()
co2 = data.copy()

for j in range(len(oxygen[0])):
    one_or_zero = 0
    for i in range(len(oxygen)):
        if oxygen[i][j] == '1':
            one_or_zero += 1
        else:
            one_or_zero -= 1

    if one_or_zero >= 0:
        oxygen = [ x for x in oxygen if x[j] == '1' ]
    else:
        oxygen = [ x for x in oxygen if x[j] == '0' ]
    if len(oxygen) == 1:
        break

for j in range(len(co2[0])):
    one_or_zero = 0
    for i in range(len(co2)):
        if co2[i][j] == '1':
            one_or_zero += 1
        else:
            one_or_zero -= 1

    if one_or_zero >= 0:
        co2 = [ x for x in co2 if x[j] == '0' ]
    else:
        co2 = [ x for x in co2 if x[j] == '1' ]
    if len(co2) == 1:
        break

# Binary to dec
print(oxygen, co2)
print(int(oxygen[0], 2) * int(co2[0], 2))