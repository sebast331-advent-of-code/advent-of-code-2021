import numpy as np
import numpy.ma as ma
from numpy.ma.core import masked
import sys

with open('input.txt') as f:
    data = f.read().splitlines()


class BingoCard():
    def __init__(self, numbers):
        self._board = np.array([
            numbers[0:5],
            numbers[5:10],
            numbers[10:15],
            numbers[15:20],
            numbers[20:25]], int
        )
        self._mask = [0 for i in range(25)]

    def is_winner(self):
        masked_board = ma.masked_array(self._board, mask=self._mask)

        # Horizontal
        for arr in masked_board:
            if all([x is ma.masked for x in arr]):
                return True

        # Vertical
        for arr in masked_board.T:
            if all([x is ma.masked for x in arr]):
                return True

        # # Diagonals
        # if all([
        #     masked_board[0, 0] is ma.masked,
        #     masked_board[1, 1] is ma.masked,
        #     masked_board[2, 2] is ma.masked,
        #     masked_board[3, 3] is ma.masked,
        #     masked_board[4, 4] is ma.masked
        # ]):
        #     return True

        # if all([
        #     masked_board[0, 4] is ma.masked,
        #     masked_board[1, 3] is ma.masked,
        #     masked_board[2, 2] is ma.masked,
        #     masked_board[3, 1] is ma.masked,
        #     masked_board[4, 0] is ma.masked
        # ]):
        #     return True

        return False

    def add_number(self, number):
        for i, arr in enumerate(self._board):
            for j, num in enumerate(arr):
                if num == number:
                    self._mask[i*5+j] = 1
                    return
    @property
    def score(self):
        masked_board = ma.masked_array(self._board, mask=self._mask)
        return np.sum(masked_board)

    def __repr__(self) -> str:
        return str(self._board)


tirage = [ int(i) for i in data[0].split(',') ]
all_cards = []
for i in range(2, len(data), 6):
    line = data[i]
    # Blank lines
    if line == '':
        continue

    # Create a new card
    card_numbers = data[i].split(' ') + data[i+1].split(' ') + \
        data[i + 2].split(' ') + data[i+3].split(' ') + data[i+4].split(' ')
    card_numbers = [x for x in card_numbers if x != '']
    all_cards.append(BingoCard(card_numbers))


for i, num in enumerate(tirage):
    for card in all_cards:
        card.add_number(num)
        if card.is_winner():
            print(f"Score de la carte gagnante: {card.score * num}")
            print(f"Carte gagnante:\n{card}")
            break
    else:
        continue
    break