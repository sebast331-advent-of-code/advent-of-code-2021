import numpy as np

with open('input.txt') as f:
    data = f.read().splitlines()


def get_max_coords(arr):
    max_x = 0
    max_y = 0
    for fr, to in arr:
        max_x = max(max_x, fr[0], to[0])
        max_y = max(max_y, fr[1], to[1])
    return max_x+1, max_y+1

# Parse
coords = []
for line in data:
    spl = line.split(' -> ')
    new = list(map(int, spl[0].split(','))), list(map(int, spl[1].split(',')))
    coords.append( new )

plan = np.zeros(get_max_coords(coords))

# Make the plan
for coord in coords:
    fr, to = coord
    fr_x, fr_y = fr
    to_x, to_y = to
    # Horizontal
    if fr_x == to_x:
        for i in range(min(fr_y, to_y), max(fr_y, to_y)+1):
            plan[i, fr_x] += 1
            pass
    # Vertical
    elif fr_y == to_y:
        for i in range(min(fr_x, to_x), max(fr_x, to_x)+1):
            plan[fr_y, i] += 1
            pass
    # Diagonal
    else:
        add_x = -1 if fr_x > to_x else 1
        add_y = -1 if fr_y > to_y else 1
        for i in range(min(fr_x, to_x), max(fr_x, to_x)+1):
            plan[fr_y, fr_x] += 1
            fr_x += add_x
            fr_y += add_y
            

# Count the amount of 2+
amount = sum([1 for line in plan for num in line if num > 1])
print(amount)