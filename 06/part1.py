with open('input.txt') as f:
    data = f.read()
    data = [int(i) for i in data.split(',')]

DAYS = 80
RESET_DAYS = 6
NEW_DAYS = 8

for _ in range(DAYS):
    new_fish = data.count(0)
    data = [i-1 if i > 0 else RESET_DAYS for i in data]
    data = data + [NEW_DAYS for _ in range(new_fish)]

print(f"Total: {len(data)} fishes")
    