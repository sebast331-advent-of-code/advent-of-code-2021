from collections import defaultdict

from numpy import newaxis


with open('input.txt') as f:
    data = f.read()
    data = [int(i) for i in data.split(',')]

DAYS = 256
RESET_DAYS = 6
NEW_DAYS = 8

fishes = defaultdict(lambda: 0)
for i in data:
    fishes[i] = data.count(i)

for _ in range(DAYS):
    # Decrement
    for i in range(-1, 9):
        fishes[i] = fishes[i+1]
    # Reset fishes
    fishes[RESET_DAYS] += fishes[-1]
    # New fishes
    fishes[NEW_DAYS] = fishes[-1]    

fishes[-1] = 0
total_fishes = sum([x for x in fishes.values()])
print(f"Total: {total_fishes} fishes")
    