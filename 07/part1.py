with open('input.txt') as f:
    data = f.read()
    data = [int(i) for i in data.split(',')]

def calc_fuel(arr, pos):
    fuel = 0
    for num in arr:
        fuel += abs(pos - num)
    return fuel
    
lower_pos = None
lower_cost = None
for i in range(min(data), max(data)+1):
    cost = calc_fuel(data, i)
    if type(lower_pos) is not int or cost < lower_cost:
        lower_pos = i
        lower_cost = cost

print(f"Lower fuel cost is {lower_cost} at position {lower_pos}")