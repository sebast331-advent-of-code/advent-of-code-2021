with open('input.txt') as f:
    data = f.read().splitlines()

count = 0
for line in data:
    digits = line.split(' | ')[1]
    count += len(list(filter(lambda x: len(x) in [7, 4, 3, 2], digits.split(' '))))

print(f"Digits 1, 4, 7 and 8 appear a total of {count} times")