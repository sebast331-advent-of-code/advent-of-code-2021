from itertools import permutations
from collections import defaultdict

with open('input.txt') as f:
    data = f.read().splitlines()

d_digits = {
    0: 'abcefg',    # 6
    1: 'cf',        # 2 - Unique
    2: 'acdeg',     # 5
    3: 'acdfg',     # 5
    4: 'bcdf',      # 4 - Unique
    5: 'abdfg',     # 5
    6: 'abdefg',    # 6
    7: 'acf',       # 3 - Unique
    8: 'abcdefg',   # 7 - Unique
    9: 'abcdfg'     # 6
}

alpha = {
    'a': 0,
    'b': 1,
    'c': 2,
    'd': 3,
    'e': 4,
    'f': 5,
    'g': 6
}

def do_permutation(key):
    test_digits = defaultdict(lambda: '')
    for k, v in d_digits.items():
        for idx, letter in enumerate(v):
            test_digits[k] += key[alpha[letter]]
        test_digits[k] = sorted(test_digits[k])
    return test_digits

def get_digits_value(digits, key):
    number = ''
    for digit in digits.split(' '):
        sorted_digit = sorted(digit)
        for k, v in enumerate(key.values()):
            if sorted(v) == sorted_digit:
                number += str(k)
    # print(number, int(number), digits)
    return int(number)

possibilities = list(permutations('abcdefg'))
total = 0
num = 0
for possibility in possibilities:
    test_digits = do_permutation(possibility)

    for line in data:
        letters, digits = line.split(' | ')
        if all([ True if sorted(single_digit) in list(test_digits.values()) else False for single_digit in letters.split(' ') ]):
            # WIN!
            num += 1
            total += get_digits_value(digits, test_digits)

print(f"Sum of all digits: {total} - {num}")