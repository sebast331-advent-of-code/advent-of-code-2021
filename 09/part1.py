import copy
import numpy as np

with open('input.txt') as f:
    data = f.read().splitlines()
    data = [ list(d) for d in data ]

# Create the map
arr = np.array(data, dtype=int)
shape = (arr.shape[0]+2, arr.shape[1]+2)
heightmap = np.full(shape, 99)
heightmap[1:-1, 1:-1] = arr
tmp_map = copy.deepcopy(heightmap)

# Find lower points
for i in range(1, len(heightmap) - 1):
    for j in range(1, len(heightmap[i]) - 1):
        # # Ignore 99
        if tmp_map[i][j] == 99:
            continue
        # Check 4 points
        if tmp_map[i][j] >= tmp_map[i-1][j]: heightmap[i][j] = 99
        if tmp_map[i][j] >= tmp_map[i+1][j]: heightmap[i][j] = 99
        if tmp_map[i][j] >= tmp_map[i][j-1]: heightmap[i][j] = 99
        if tmp_map[i][j] >= tmp_map[i][j+1]: heightmap[i][j] = 99
        
# Calculate the risk level
risk_level = sum(heightmap[heightmap != 99] + 1)
print(f"The sum of risk level is {risk_level}")