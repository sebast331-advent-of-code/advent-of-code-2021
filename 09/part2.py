import math
import numpy as np

# Returns: array of visited
# Param visited: by_ref
# Recursive
def get_bassin(map, coord, visited=None):
    # Not a bassin
    if map[coord] == 0: return 0
    # Defaults
    if visited == None:
        visited = []
    # Coordinates
    coords = [
        (coord[0] + 1, coord[1]),
        (coord[0] - 1, coord[1]),
        (coord[0], coord[1] + 1),
        (coord[0], coord[1] - 1)
    ]
    for new_coord in coords:
        if new_coord not in visited:
            if map[new_coord]:
                visited.append(new_coord)
                get_bassin(map, new_coord, visited)
    return visited


with open('input.txt') as f:
    data = f.read().splitlines()
    data = [ list(d) for d in data ]

# Create the map
arr = np.array(data, dtype=int)
shape = (arr.shape[0]+2, arr.shape[1]+2)
heightmap = np.full(shape, 9)
heightmap[1:-1, 1:-1] = arr
bassins = np.full(shape, False)

# Find lower points
for i in range(1, len(heightmap) - 1):
    for j in range(1, len(heightmap[i]) - 1):
        # # Ignore 9
        if heightmap[i][j] == 9:
            continue
        # Everything is a bassin except 9 of heights
        bassins[i][j] = True

# Calculate bassins
bassins_visited = []
bassins_sizes = []
for i in range(1, len(bassins)):
    for j in range(1, len(bassins)):
        # Do not revisit a bassin
        if (i, j) in bassins_visited:
            continue
        # Get the bassin and calculate
        if bassins[i][j]:
            bassin = get_bassin(bassins, (i, j))
            bassins_visited += bassin
            bassins_sizes.append(len(bassin))
        
bassins_sizes.sort(reverse=True)
print(f"3 largest bassins = {math.prod(bassins_sizes[0:3])}")