# Débogage
J'ai eu un problème avec mes bassins. Pour déboguer, j'ai généré une carte de mes bassins
et une *heathmap* dans LibreOffice Calc (Excel).

J'ai remarqué que le point AG12, qui aurait dû être un bassin (fichier `map.ods`), ne l'était pas (fichier `bassins.ods`).