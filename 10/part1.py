with open('input.txt') as f:
    data = f.read().splitlines()

assoc = {
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<'
}
points = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}
invalids = []

str_openings = '([{<'
str_closing = ')]}>'


for line in data:
    stack = []
    for c in line:
        if c in str_openings:
            stack.append(c)
        else:
            opening = stack.pop()
            if assoc[c] != opening:
                invalids.append(c)
                break

score = 0
for char in invalids:
    score += points[char]

print(f"Final score: {score}")