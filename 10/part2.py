from collections import defaultdict

with open('input.txt') as f:
    data = f.read().splitlines()

assoc = {
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<'
}
points = {
    '(': 1,
    '[': 2,
    '{': 3,
    '<': 4
}
missings = []

str_openings = '([{<'
str_closing = ')]}>'

# Discard the corrupted ones
for line in data.copy():
    stack = []
    for c in line:
        if c in str_openings:
            stack.append(c)
        else:
            opening = stack[-1]
            if assoc[c] != opening:
                # Corrupted
                data.remove(line)
                break
            else:
                stack.pop()
    else:
        missings.append(stack)

# Calculate the score
totals = []
for line in missings:
    score = 0
    while len(line):
        c = line.pop()
        score *= 5
        score += points[c]
    totals.append(score)

totals.sort()
print(f"Total middle score is {totals[int(len(totals)/2)]}")