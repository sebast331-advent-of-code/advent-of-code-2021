import numpy as np
import copy
import math

with open('input.txt') as f:
    data = f.read().splitlines()
    data = [list(i) for i in data]
    data = np.array(data, dtype=float)

num_step = 0
count_flash = 0

while True:
    num_step += 1
    # Flash
    data += 1

    # Increase if 9 is nearby
    while(any(data[data > 9])):
        tmp_data = copy.deepcopy(data)
        for y in range(len(data)):
            for x in range(len(data[0])):
                # Already exploded
                if data[x][y] >= 10:
                    tmp_data[x][y] = -math.inf
                    count_flash += 1
                # Check around
                if x > 0 and y > 0 and data[x-1][y-1] > 9: tmp_data[x][y] += 1
                if y > 0 and data[x][y-1] > 9: tmp_data[x][y] += 1
                if x+1 < len(data[0]) and y > 0 and data[x+1][y-1] > 9: tmp_data[x][y] += 1
                if x > 0 and data[x-1][y] > 9: tmp_data[x][y] += 1
                if x+1 < len(data[0]) and data[x+1][y] > 9: tmp_data[x][y] += 1
                if x > 0 and y+1 < len(data) and data[x-1][y+1] > 9: tmp_data[x][y] += 1
                if y+1 < len(data) and data[x][y+1] > 9: tmp_data[x][y] += 1
                if x+1 < len(data[0]) and y+1 < len(data) and data[x+1][y+1] > 9: tmp_data[x][y] += 1
        data = tmp_data

    # Reset exploded
    data[data == -math.inf] = 0

    if np.all(data == np.zeros(data.shape)):
        break

print(f"Sync step: {num_step}")